using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;

namespace exemploDapperCrud.Repositories
{
    public abstract class AbstractRepository<T> where T: class, new()
    {
        protected readonly IDbConnection _connection;

        public AbstractRepository(IDbConnection configuration)
        {
            _connection = configuration;
        }

        public long Add(T obj)
        {
            return this._connection.Insert(obj);
        }

        public void Remove(int id)
        {
            var remove = this._connection.Delete<T>(FindById(id));
        }

        public void Remove(T item)
        {
            var remove = this._connection.Delete<T>(item);
        }

        public void Update(T item)
        {
            var update = this._connection.Update<T>(item);
        }

        public T FindById(int id)
        {
            return _connection.Get<T>(id);
        }

        public IEnumerable<T> FindAll()
        {
            return _connection.GetAll<T>().ToList();
        }
    }
}